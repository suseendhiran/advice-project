import React, { useEffect, useState } from "react";
import axios from "axios";

import "./App.css";

function App() {
  const [fetchedAdvice, setFetchedadvice] = useState("");
  const [isLoading, setIsLoading] = useState(true);
  const [prevAdvice, setPrevAdvice] = useState("");
  const fetchAdvice = () => {
    console.log("Executing");
    axios
      .get("https://api.adviceslip.com/advice")
      .then((response) => {
        console.log("inside response");
        const { advice } = response.data.slip;
        setFetchedadvice(advice);
        console.log(advice);
        if (prevAdvice == advice) {
          setIsLoading(true);
          console.log("inside previous");
          return fetchAdvice();
        }
        setPrevAdvice(advice);
        setIsLoading(false);
      })
      .catch((err) => {
        console.log(err);
      });
  };
  useEffect(() => {
    fetchAdvice();
  }, []);
  console.log(isLoading);
  return (
    <div className="app">
      <div className="card">
        {isLoading ? (
          <p>Loading...</p>
        ) : (
          <h1 className="heading">{fetchedAdvice}</h1>
        )}

        <button className="button" onClick={fetchAdvice}>
          Give me Advice!
        </button>
      </div>
    </div>
  );
}

export default App;
